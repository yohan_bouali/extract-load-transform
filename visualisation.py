import streamlit as st
import pandas as pd

st.write("""#Data visualisation""")

df = pd.read_csv("data/Query1.csv")
df = df.set_index('annee_tour_scrutin')
st.line_chart(df)