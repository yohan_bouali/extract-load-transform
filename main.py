import glob
import os
import psycopg2
from datetime import datetime
from csv import reader

c = psycopg2.connect("dbname=bdmprojet user=checkadmin password=toto host=172.31.253.210 port=5432")


def get_month(mois):
    if mois == 'janvier':
        mois = 1
    if mois == 'février':
        mois = 2
    if mois == 'mars':
        mois = 3
    if mois == 'avril':
        mois = 4
    if mois == 'mai':
        mois = 5
    if mois == 'juin':
        mois = 6
    if mois == 'juillet':
        mois = 7
    if mois == 'août':
        mois = 8
    if mois == 'septembre':
        mois = 9
    if mois == 'octobre':
        mois = 10
    if mois == 'novembre':
        mois = 11
    if mois == 'décembre':
        mois = 12
    return mois


def ingest_in_location():
    print("-------------------INSERTION dans la table Lieu-------------------")
    cur = c.cursor()
    cur.execute('TRUNCATE TABLE "Lieu" RESTART IDENTITY CASCADE ')
    cur.execute('TRUNCATE TABLE "Lieu_MAX" RESTART IDENTITY CASCADE ')
    for csv in list_of_csv:
        if csv != 'departements-region.csv' and csv != 'ptendance.csv':
            with open(csv, 'r', encoding='utf-8') as read_obj:
                df = reader(read_obj)
                header = next(df)
                if header is not None:
                    for row in df:
                        print('insert into "Lieu_MAX" (code, circonscription)' + " values ('"
                                    + str(row[0]) + "',"
                                    + "'" + str(row[2]) + "')")
                        cur.execute('insert into "Lieu_MAX" (code, circonscription)' + "values ('"
                                    + str(row[0]) + "',"
                                    + "'" + str(row[2]) + "')")

    for csv in list_of_csv:
        if csv == 'departements-region.csv':
            with open(csv, 'r', encoding='utf-8') as read_obj:
                df = reader(read_obj)
                header = next(df)
                if header is not None:
                    for row in df:
                        code = row[0]
                        departement = row[1]
                        region = row[2]
                        departement = departement.replace("'", " ")
                        region = region.replace("'", " ")
                        cur.execute("select max(circonscription) from \"Lieu_MAX\" where code='" + code + "'")
                        max = cur.fetchone()[0]
                        print("MAX FOR code : " + code + " is " + str(max))
                        if max is None:
                            max = 1
                        for i in range(0, max):
                            print('insert into "Lieu" (code, departement, region, circonscription)' + "values ('"
                                  + str(code) + "',"
                                  + "'" + str(departement) + "',"
                                  + "'" + str(region) + "',"
                                  + "'" + str(i + 1) + "')")

                            cur.execute('insert into "Lieu" (code, departement, region, circonscription)' + "values ('"
                                        + str(code) + "',"
                                        + "'" + str(departement) + "',"
                                        + "'" + str(region) + "',"
                                        + "'" + str(i + 1) + "')")

    for i in range(1, 9):
        cur.execute("UPDATE \"Lieu_MAX\" SET code = '0" + str(i) + "' WHERE code = '" + str(i) + "'")
        cur.execute("UPDATE \"Lieu\" SET code = '0" + str(i) + "' WHERE code = '" + str(i) + "'")

    c.commit()


def ingest_in_ballot():
    print("-------------------INSERTION dans la table Scrutin-------------------")
    cur = c.cursor()
    cur.execute('TRUNCATE TABLE "Scrutin" RESTART IDENTITY CASCADE ')
    print("Insert tour 1")
    cur.execute('insert into "Scrutin" (type_scrutin, tour_scrutin)' + "values ('"
                + str("Présidentielle") + "',"
                + "'" + str("Tour 1") + "')")
    print("Insert tour 2")
    cur.execute('insert into "Scrutin" (type_scrutin, tour_scrutin)' + "values ('"
                + str("Présidentielle") + "',"
                + "'" + str("Tour 2") + "')")
    c.commit()


def ingest_in_date():
    print("-------------------INSERTION dans la table Date-------------------")
    cur = c.cursor()
    cur.execute('TRUNCATE TABLE "Date" RESTART IDENTITY CASCADE ')
    for txt in list_of_txt:
        with open(txt, 'r', encoding='utf-8') as read_obj:
            first_line_of_txt = read_obj.readline()
            result_index = first_line_of_txt.find('(')
            date = first_line_of_txt[result_index:]
            date = date.replace('(', ' ')
            date = date.replace(')', ' ')
            date = date.split(' ')
            jour = date[1]
            jour = jour.replace("er", "")
            mois = date[2]
            annee = date[3]
            mois = get_month(mois)
            print(str(jour) + '/' + str(mois) + '/' + str(annee))
            cur.execute('insert into "Date" (jour, mois, annee)' + "values ("
                        + str(jour) + ","
                        + str(mois) + ","
                        + str(annee) + ")")
            c.commit()


def ingest_in_candidate():
    print("-------------------INSERTION dans la table Candidat-------------------")
    cur = c.cursor()
    cur.execute('TRUNCATE TABLE "Candidat" RESTART IDENTITY CASCADE ')
    candidates = []
    for csv in list_of_csv:
        if csv != 'departements-region.csv' and csv != 'ptendance.csv':
            with open(csv, 'r', encoding='utf-8') as read_obj:
                df = reader(read_obj)
                header = next(df)
                for i in range(7, len(header)):
                    candidate = header[i]
                    index = candidate.find('(')
                    parti = candidate[index - 1:]
                    parti = parti.replace('(', '').replace(')', '')
                    if parti.strip() != "":
                        print(str(parti.strip()))
                        cur.execute("SELECT id_parti FROM \"Parti\" WHERE NOM_PARTI = '" + parti.strip() + "'")
                        id = cur.fetchone()[0]
                        tuple = (candidate[0:index - 1].replace("'", " "), id)
                        candidates.append(tuple)
                    else:
                        cur.execute("SELECT id_parti FROM \"Parti\" WHERE NOM_PARTI = 'Sans Parti'")
                        id = cur.fetchone()[0]
                        tuple = (candidate[0:index - 1].replace("'", " "), id)
                        candidates.append(tuple)

    candidates_without_duplicate = []
    for i in candidates:
        if i not in candidates_without_duplicate:
            candidates_without_duplicate.append(i)

    for candidate in candidates_without_duplicate:
        cur.execute('insert into "Candidat" (nom_candidat, id_parti)' + "values ('" + str(candidate[0]) + "','" + str(
            candidate[1]) + "')")
        c.commit()


def ingest_in_party():
    print("-------------------INSERTION dans la table Parti-------------------")
    cur = c.cursor()
    cur.execute('TRUNCATE TABLE "Parti" RESTART IDENTITY CASCADE')
    cur.execute(
        'insert into "Parti" (nom_parti, tendance)' + "values ('" + str('Sans Parti') + "','" + str(
            'Pas de tendance') + "')")
    partys = []
    tendancies = []
    for csv in list_of_csv:
        if csv != 'departements-region.csv':
            with open(csv, 'r', encoding='utf-8') as read_obj:
                df = reader(read_obj)
                header = next(df)
                for i in range(7, len(header)):
                    party = header[i]
                    index = party.find('(')
                    partys.append(party[index:])
                partys.sort()

    partys_without_duplicate = []
    for i in partys:
        if i not in partys_without_duplicate:
            partys_without_duplicate.append(i)

    for party in partys_without_duplicate:
        if party != ")":
            party = party.replace("(", "")
            party = party.replace(")", "")
            # print(party)
            with open(path + '/ptendance.csv', 'r', encoding='utf-8') as tendance:
                for row in tendance:
                    if row.split(',')[0] == party:
                        print(row)
                        print(party + '---' + row.split(',')[1])
                        tendancies.append(row.split(',')[1])
                        cur.execute(
                            'insert into "Parti" (nom_parti, tendance)' + "values ('" + str(party) + "','" + str(
                                row.split(',')[1].strip()) + "')")
            c.commit()


def ingest_in_result():
    cur = c.cursor()
    cur.execute('TRUNCATE TABLE "Resultat" RESTART IDENTITY')
    for txt in list_of_txt:
        id_tour = 0
        if txt.__contains__("t1"):
            id_tour = 1
        elif txt.__contains__("t2"):
            id_tour = 2
        with open(txt, 'r', encoding='utf-8') as read_obj:
            first_line_of_txt = read_obj.readline()
            result_index = first_line_of_txt.find('(')
            date = first_line_of_txt[result_index:]
            date = date.replace('(', ' ')
            date = date.replace(')', ' ')
            date = date.split(' ')
            jour = date[1]
            jour = jour.replace("er", "")
            mois = date[2]
            annee = date[3]
            mois = get_month(mois)
            print(str(jour) + "/" + str(mois) + "/" + str(annee))
            cur.execute(
                "select id_date from \"Date\" where jour = " + str(jour) + " and mois =" + str(
                    mois) + " and annee =" + str(annee))
            id_date = cur.fetchone()[0]
            for csv in list_of_csv:
                if csv.split(".")[0] == txt.split(".")[
                    0] and csv != 'departements-region.csv' and csv != 'ptendance.csv':
                    with open(csv, 'r', encoding='utf-8') as read_obj:
                        df = reader(read_obj)
                        header = next(df)
                        for i in range(7, len(header)):
                            candidate = header[i]
                            index = candidate.find('(')
                            parti = candidate[index - 1:]
                            parti = parti.replace('(', '').replace(')', '')
                            candidate = candidate[0:index - 1].replace("'", " ")
                            if parti.strip() != "":
                                cur.execute(
                                    "SELECT id_parti FROM \"Parti\" WHERE NOM_PARTI = '" + parti.strip() + "'")
                                id_parti = cur.fetchone()[0]
                                cur.execute(
                                    "select id_candidat from \"Candidat\" where nom_candidat = '" + str(
                                        candidate.strip()) + "' and id_parti = " + str(id_parti))
                                id_candidat = cur.fetchone()[0]
                            else:
                                cur.execute("SELECT id_parti FROM \"Parti\" WHERE NOM_PARTI = 'Sans Parti'")
                                id_parti = cur.fetchone()[0]
                                cur.execute("select id_candidat from \"Candidat\" where nom_candidat = '" + str(
                                    candidate.strip()) + "' and id_parti = " + str(id_parti))
                                id_candidat = cur.fetchone()[0]
                            with open(csv, 'r', encoding='utf-8') as read_obj:
                                df = reader(read_obj)
                                header = next(df)
                                if header is not None:
                                    for row in df:
                                        nb_vote = row[i]
                                        nb_vote_exprime = row[5]
                                        code = int(row)
                                        print(str(row[0]) + " code and circonscription " + str(row[2]))
                                        cur.execute("select id_lieu from \"Lieu\" where code LIKE '" + str(
                                            code) + "' and circonscription = " + str(row[2]))
                                        id_lieu = cur.fetchone()[0]
                                        print(candidate + " " + nb_vote + " " + str(nb_vote_exprime))
                                        cur.execute(
                                            "insert into \"Resultat\" (id_candidat, id_date, id_scrutin, id_lieu, nb_votes, nb_votes_exprimes) values(" + str(
                                                id_candidat) + "," + str(id_date) + "," + str(id_tour) + "," + str(
                                                id_lieu) + "," + str(nb_vote) + "," + str(nb_vote_exprime) + ")");
                                        c.commit()


def set_code(row):
    if row[0] == "1":
        print("dans le if")
        row[0] = "01"
    if row[0] == "2":
        print("dans le if")
        row[0] = "02"
    if row[0] == "3":
        print("dans le if")
        row[0] = "03"
    if row[0] == "4":
        print("dans le if")
        row[0] = "04"
    if row[0] == "5":
        print("dans le if")
        row[0] = "05"
    if row[0] == "6":
        print("dans le if")
        row[0] = "06"
    if row[0] == "7":
        print("dans le if")
        row[0] = "07"
    if row[0] == "8":
        print("dans le if")
        row[0] = "08"
    if row[0] == "9":
        print("dans le if")
        row[0] = "09"
    return row[0]


def ingest_in_participation():
    cur = c.cursor()
    cur.execute('TRUNCATE TABLE "Participation" RESTART IDENTITY')
    for txt in list_of_txt:
        id_tour = 0
        if txt.__contains__("t1"):
            id_tour = 1
        elif txt.__contains__("t2"):
            id_tour = 2
        with open(txt, 'r', encoding='utf-8') as read_obj:
            first_line_of_txt = read_obj.readline()
            result_index = first_line_of_txt.find('(')
            date = first_line_of_txt[result_index:]
            date = date.replace('(', ' ')
            date = date.replace(')', ' ')
            date = date.split(' ')
            jour = date[1]
            jour = jour.replace("er", "")
            mois = date[2]
            annee = date[3]
            mois = get_month(mois)
            print(str(jour) + "/" + str(mois) + "/" + str(annee))
            cur.execute(
                "select id_date from \"Date\" where jour = " + str(jour) + " and mois =" + str(
                    mois) + " and annee =" + str(annee))
            id_date = cur.fetchone()[0]
            for csv in list_of_csv:
                if csv.split(".")[0] == txt.split(".")[0] and csv != 'departements-region.csv' and csv != 'ptendance.csv':
                    with open(csv, 'r', encoding='utf-8') as read_obj:
                        df = reader(read_obj)
                        header = next(df)
                        for i in range(7, len(header)):
                            if header is not None:
                                for row in df:
                                    nb_votants = row[4]
                                    nb_exprimes = row[5]
                                    nb_blancs_nuls = row[6]
                                    nb_inscrits = row[3]
                                    nb_abstention = int(int(nb_inscrits) - int(nb_votants))
                                    code = int(row)
                                    cur.execute("select id_lieu from \"Lieu\" where code LIKE '" + str(
                                        code) + "' and circonscription = " + str(row[2]))
                                    id_lieu = cur.fetchone()[0]
                                    print(
                                        "INSERT INTO \"Participation\" (id_scrutin, id_date, id_lieu, nb_votants, nb_exprimes, nb_blancs_nuls,nb_inscrits, nb_abstention) values(" + str(
                                            id_tour) + "," + str(id_date) + "," + str(id_lieu) + ",'" + str(
                                            nb_votants) + "','" + str(nb_exprimes) + "','" + str(
                                            nb_blancs_nuls) + "','" + str(nb_inscrits) + "','" + str(
                                            nb_abstention) + "')")
                                    cur.execute(
                                        "INSERT INTO \"Participation\" (id_scrutin, id_date, id_lieu, nb_votants, nb_exprimes, nb_blancs_nuls,nb_inscrits, nb_abstention) values(" + str(
                                            id_tour) + "," + str(id_date) + "," + str(id_lieu) + ",'" + str(
                                            nb_votants) + "','" + str(nb_exprimes) + "','" + str(
                                            nb_blancs_nuls) + "','" + str(nb_inscrits) + "','" + str(
                                            nb_abstention) + "')")
                                    c.commit()


print("DEBUT : " + str(datetime.now()))
debut = str(datetime.now())
path = r'/Users/yohan/IdeaProjects/extract-load-transform/data'
extension_csv = 'csv'
extension_txt = 'txt'
os.chdir(path)
list_of_csv = glob.glob('*.{}'.format(extension_csv))  # list avec le nom de chaque csv
list_of_csv.sort()
print(list_of_csv)
list_of_txt = glob.glob('*.{}'.format(extension_txt))  # list avec le nom de chaque txt
list_of_txt.sort()
print(list_of_csv)
ingest_in_location()
ingest_in_ballot()
ingest_in_date()
ingest_in_party()
ingest_in_candidate()
ingest_in_result()
ingest_in_participation()
fin = str(datetime.now())
print("DEBUT : " + debut)
print("FIN : " + fin)
