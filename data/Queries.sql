--Query 1 --bar chart

SELECT cast((sum(nb_votants)::float / sum(nb_inscrits)::float) as float)*100 as participation, annee, tour_scrutin
FROM "Participation" p
         INNER JOIN "Date" d ON
        p.id_date = d.id_date
INNER JOIN "Scrutin" S on S.id_scrutin = p.id_scrutin
group by annee, tour_scrutin
order by annee


--Query 2 --line chart

SELECT region, cast((sum(nb_votants)::float / sum(nb_inscrits)::float) as float)*100 as participation, annee, tour_scrutin
FROM "Participation" p
         INNER JOIN "Date" d ON
        p.id_date = d.id_date
         INNER JOIN "Scrutin" S on S.id_scrutin = p.id_scrutin
         INNER JOIN "Lieu" L on L.id_lieu = p.id_lieu
group by region, annee, tour_scrutin
order by region, annee, tour_scrutin

-- Query 3 --bar chart

SELECT sum(sum_score), tendance, annee FROM (SELECT distinct  tendance, sum(score) as sum_score, annee FROM (SELECT cast((sum(nb_votes)::float / sum(nb_votes_exprimes)::float) as float)*100 as score, annee, nom_candidat, tendance
                                                   FROM "Resultat" r
                                                            INNER JOIN "Date" d ON r.id_date = d.id_date
                                                            INNER JOIN "Scrutin" S on S.id_scrutin = r.id_scrutin
                                                            INNER JOIN "Candidat" C on r.id_candidat = C.id_candidat
                                                            INNER JOIN "Parti" P on C.id_parti = P.id_parti
                                                   WHERE tour_scrutin = 'Tour 1'
                                                   group by tendance, nom_candidat, annee
                                                   order by annee) as result
group by tendance, annee, score) as req
group by tendance, annee
order by annee

--Query 4 --line chart

SELECT sum(sum_score), tendance, annee, region FROM (SELECT distinct  tendance, sum(score) as sum_score, annee, region FROM (SELECT cast((sum(nb_votes)::float / sum(nb_votes_exprimes)::float) as float)*100 as score, region, annee, nom_candidat, tendance
                                                   FROM "Resultat" r
                                                            INNER JOIN "Date" d ON r.id_date = d.id_date
                                                            INNER JOIN "Scrutin" S on S.id_scrutin = r.id_scrutin
                                                            INNER JOIN "Candidat" C on r.id_candidat = C.id_candidat
                                                            INNER JOIN "Parti" P on C.id_parti = P.id_parti
                                                            INNER JOIN "Lieu" L on L.id_lieu = r.id_lieu
                                                   WHERE tour_scrutin = 'Tour 1'
                                                   group by tendance, nom_candidat, annee, region
                                                   order by annee) as result
group by tendance, annee, score, region) as req
group by tendance, annee, region
order by annee

--Query 5 --tableau

SELECT avg(score), departement FROM (SELECT cast((sum(nb_votes)::float / sum(nb_votes_exprimes)::float) as float)*100 as score, nom_candidat, departement
FROM "Resultat" r
         INNER JOIN "Date" d ON r.id_date = d.id_date
         INNER JOIN "Scrutin" S on S.id_scrutin = r.id_scrutin
         INNER JOIN "Candidat" C on r.id_candidat = C.id_candidat
         INNER JOIN "Parti" P on C.id_parti = P.id_parti
         INNER JOIN "Lieu" L on L.id_lieu = r.id_lieu
WHERE tour_scrutin = 'Tour 1' and CONCAT(nom_candidat,'-', annee) IN (SELECT CONCAT(nom_candidat,'-', annee)
                                                            FROM "Candidat"
                                                            INNER JOIN "Resultat" R on "Candidat".id_candidat = R.id_candidat
                                                            INNER JOIN "Scrutin" S on S.id_scrutin = R.id_scrutin
                                                            INNER JOIN "Date" D on D.id_date = R.id_date
                                                   WHERE tour_scrutin = 'Tour 2'
                                                   GROUP by nom_candidat, annee)
GROUP BY departement, nom_candidat) sub
GROUP By departement

--Query 6 --tableau

SELECT avg(score) as moyenne, region FROM (SELECT cast((sum(nb_votes)::float / sum(nb_votes_exprimes)::float) as float)*100 as score, region
FROM "Candidat"
         INNER JOIN "Resultat" R on "Candidat".id_candidat = R.id_candidat
         INNER JOIN "Scrutin" S on S.id_scrutin = R.id_scrutin
         INNER JOIN "Date" D on D.id_date = R.id_date
         INNER JOIN "Lieu" L on L.id_lieu = R.id_lieu
WHERE nom_candidat = 'LAGUILLER'
GROUP by annee, region) as sub
GROUP BY region
ORDER BY moyenne desc


